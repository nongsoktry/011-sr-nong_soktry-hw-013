import React, { Component } from "react";
import Axios from "axios";
let vw;
let id;
const pddleft = {
  textAlign: "left"
};
export default class View extends Component {
  constructor() {
    super();
    this.state = {
      id: "",
      data: {},
    };
  }
  componentDidMount() {
    Axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
      .then((res) => {
        this.setState({
          data: res.data.DATA,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render(props) {
    var idd = this.props.match.params.id;
    id = idd;

    return (
      <div className="container-fluid" style={pddleft}>
        <h2>Article Content View</h2> {vw}
        <div className="row">
          <div className="col-lg-4">
            <img
              src={
                this.state.data.IMAGE === null
                  ? "https://www.w3schools.com/howto/img_avatar.png"
                  : this.state.data.IMAGE
              }
              alt="image article not shown"
              width="100%"
            />
          </div>
          <div className="col-lg-8">
            <h1>{this.state.data.TITLE}</h1>
            <h3>{this.state.data.DESCRIPTION}</h3>
          </div>
        </div>
      </div>
    );
  }
}
