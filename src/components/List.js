import React, { Component } from "react";
import { BrowserRouter as Link } from "react-router-dom";

export default class List extends Component {
  constructor() {
    super();
    this.substring = this.substring.bind(this);
    this.getDelete = this.getDelete.bind(this);
  }

  substring = (att) => {
    var year = att.substring(0, 4);
    var month = att.substring(4, 6);
    var day = att.substring(6, 8);
    var date = day + "/" + month + "/" + year;
    var Date = [year, month, day];
    console.log(date)
    return Date.join("-");
  };

  getDelete = () => {
    this.props.delete(this.props.data.ID, this.props.index);
  };

  render(props) {
    return (
      <tr key={this.props.data.ID}>
        <td>{this.props.data.ID}</td>
        <td>{this.props.data.TITLE}</td>
        <td>{this.props.data.DESCRIPTION}</td>
        <td>{this.substring(this.props.data.CREATED_DATE)}</td>
        <td>
          <img
            src={
              this.props.data.IMAGE === null
                ? "https://www.w3schools.com/howto/img_avatar.png"
                : this.props.data.IMAGE
            }
            width="300px"
          ></img>
        </td>
        <td>
          <Link as={Link} to={`/view/${this.props.data.ID}`}>
            {" "}
            <button className="btn btn-info">view</button>
          </Link>
          <Link as={Link} to={`/add?isUpdating=true&&id=${this.props.data.ID}`}>
            <button className="btn btn-warning">edit</button>
          </Link>
          <button className="btn btn-danger" onClick={this.getDelete}>
            delete
          </button>
        </td>
      </tr>
    );
  }
}
