import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import Axios from "axios";
import query from "query-string";

export default class Add extends Component {
  constructor(props) {
    super();
    var q = query.parse(props.location.search);
    this.state = {
      isUpdating: q.isUpdating,
      url: "",
      updateid: q.id,
    };
  }
  componentWillMount() {
    if (this.state.isUpdating == "true") {
      Axios.get(
        `http://110.74.194.124:15011/v1/api/articles/${this.state.updateid}`
      )
        .then((res) => {
          this.url.value = res.data.DATA.IMAGE;
          this.title.value = res.data.DATA.TITLE;
          this.desc.value = res.data.DATA.DESCRIPTION;
          this.setState({
            url: res.data.DATA.IMAGE,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }
  updateArticle = (d) => {
    Axios.put(
      `http://110.74.194.124:15011/v1/api/articles/${this.state.updateid}`,
      {
        TITLE: d.TITLE,
        DESCRIPTION: d.DESCRIPTION,
        IMAGE: d.IMAGE,
      }
    ).then((res) => {
      console.log(res.data);
      alert(res.data.MESSAGE);
      this.props.history.push("/");
    });
  };

  addArticle = (d) => {
    Axios.post("http://110.74.194.124:15011/v1/api/articles", {
      TITLE: d.TITLE,
      DESCRIPTION: d.DESCRIPTION,
      IMAGE: d.IMAGE,
    }).then((res) => {
      console.log(res.data);
      alert(res.data.MESSAGE);
      this.props.history.push("/");
    });
  };
  onSubmit = () => {
    var url = this.url.value;
    var title = this.title.value;
    var desc = this.desc.value;
    if (url == "" || title == "" || desc == "") {
      this.description.textContent = "description can not be empty!";
      this.title.textContent = "title can not be empty!";
      this.url.textContent = "url can not be empty!";
    } else {
      this.description.textContent = "";
      this.title.textContent = "";
      this.url.textContent = "";

      let article = {
        TITLE: title,
        DESCRIPTION: desc,
        IMAGE: url,
      };
      if (this.state.isUpdating == undefined) {
        this.addArticle(article);
        this.setState({
          url: url,
        });
      } else {
        this.updateArticle(article);
        this.setState({
          url: url,
        });
      }
    }
  };
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-8" style={pddleft}>
            <h1>
              {this.state.isUpdating === undefined
                ? "Add Article"
                : "Update Article"}
            </h1>
            <Form.Group>
              <Form.Label style={mgn}>TITLE :</Form.Label>
              <Form.Control
                type="text"
                placeholder="TITLE"
                ref={(title) => (this.title = title)}
              />
              <span
                ref={(title) => (this.title = title)}
                style={{ color: "red" }}
              ></span>
              <br />
              <Form.Label style={mgn}>DESCRIPTION :</Form.Label>
              <Form.Control
                type="text"
                placeholder="DESCRIPTION"
                ref={(desc) => (this.desc = desc)}
              />
              <span
                ref={(description) => (this.description = description)}
                style={{ color: "red" }}
              ></span>
              <br />
              <Form.Label style={mgn}>IMAGE URL :</Form.Label>

              <Form.Control
                type="text"
                placeholder="IMAGE URL"
                ref={(url) => (this.url = url)}
              />
              <span
                ref={(url) => (this.url = url)}
                style={{ color: "red" }}
              ></span>
              <br />
            </Form.Group>
            <Button
              type="submit"
              className={
                this.state.isUpdating === undefined
                  ? "btn btn-dark"
                  : "btn btn-warning"
              }
              onClick={this.onSubmit}
            >
              {this.state.isUpdating === undefined ? "Submit" : "Update"}
            </Button>
          </div>
          <div className="col-4">
            <img
              src={this.state.url === "" ? imgg : this.state.url}
              alt="img"
              width="100%"
            />
          </div>
        </div>
      </div>
    );
  }
}

const mgn = {
  marginTop: "33px",
  marginButtom: "33px",
};
const pddleft = {
  textAlign: "left",
  paddingLeft: "5%",
  paddingRight: "5%",
};

const imgg = "https://www.w3schools.com/howto/img_avatar.png";
