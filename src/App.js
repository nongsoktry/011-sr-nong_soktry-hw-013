import React from "react";
import Index from "./components/homePage/Index.js";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./components/pages/Navbar";
import Add from "./components/Add.js";
import View from "./components/View.js";

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Index} />
          <Route path="/add" exact component={Add} />
          <Route path="/view/:id" exact component={View} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
